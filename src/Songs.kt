/**
 * @Description
 * Example of using objects.
 * When we run the code, the following text gets printed in the IDE’s output window:
 *Playing the song Going Underground by The Jam
 *Stopped playing Going Underground
 *«Playing the song Make Me Smile by Steve Harley»
 */
class Song(private val title: String, private val artist: String) {
    fun play() {
        println("Playing the song $title by $artist")
    }

    fun stop() {
        println("Stop playing $title")
    }
}

fun main() {
    val songOne = Song("The Mesopotamians", "They Might Be Giants")
    val songTwo = Song("Going Underground", "The Jam")
    val songThree = Song("Make Me Smile", "Steve Harley")
    songTwo.play()
    songTwo.stop()
    songThree.play()
}